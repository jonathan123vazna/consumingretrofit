package com.example.jonathanvazquez.retrofitconsuming;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.widget.Toast;

import com.example.jonathanvazquez.retrofitconsuming.adapters.PhotoAdapter;
import com.example.jonathanvazquez.retrofitconsuming.client.RetrofitClient;
import com.example.jonathanvazquez.retrofitconsuming.constants.GetDataService;
import com.example.jonathanvazquez.retrofitconsuming.model.Photo;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public PhotoAdapter adapter;
    public RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GetDataService service = RetrofitClient.getRetrofitInstance().create(GetDataService.class);
        retrofit2.Call<List<Photo>> call = service.getAllPhotos();
        call.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(retrofit2.Call<List<Photo>> call, Response<List<Photo>> response) {
                generateDataList(response.body());
            }

            @Override
            public void onFailure(retrofit2.Call<List<Photo>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Try again", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void generateDataList(List<Photo> photoList) {
        recyclerView = findViewById(R.id.photoList);
        adapter = new PhotoAdapter(this,photoList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
